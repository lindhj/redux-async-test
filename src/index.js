import React from "react";
import ReactDOM from "react-dom";
import thunkMiddleware from "redux-thunk";
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { Route } from "react-router";
import { ConnectedRouter, routerMiddleware, routerReducer } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import * as reducers from "./reducers";
import "./index.css";
import App from "./components/App";

const history = createHistory();
const reduxRoutermiddleware = routerMiddleware(history);

const composeEnhancers =
  typeof window === "object" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    }) : compose;

const enhancer = composeEnhancers(applyMiddleware(thunkMiddleware, reduxRoutermiddleware));

const reducer = combineReducers({
  ...reducers,
  routing: routerReducer
});

const store = createStore(reducer, enhancer);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Route exact path="/" component={App} />
        <Route path="/foo" component={App} />
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);
