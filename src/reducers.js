import { DOING_ASYNC, ASYNC_DONE, NEW_VAL } from "./actions";

export const doingAsyncOp = (state = false, action) => {
  switch (action.type) {
    case DOING_ASYNC:
      return true;
    case ASYNC_DONE:
      return false;
    default:
      return state;
  }
};

export const displayedValue = (state = 0, action) => {
  switch (action.type) {
    case NEW_VAL:
      return action.val;
    default:
      return state;
  }
};
