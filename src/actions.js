export const DOING_ASYNC = "DOING_ASYNC";
export const ASYNC_DONE = "ASYNC_DONE";
export const NEW_VAL = "NEW_VAL";

export const asyncDone = () => (
  {
    type: ASYNC_DONE
  }
);

export const doingAsync = () => (
  {
    type: DOING_ASYNC
  }
);

export const newVal = val => (
  {
    type: NEW_VAL,
    val
  }
);

export const fetchStuff = () => {
  const randInt = Math.floor(Math.random() * 10000);
  const twoSeconds = 2000;
  const p = new Promise(resolve => setTimeout(resolve, twoSeconds, randInt));
  return (dispatch) => {
    dispatch(doingAsync());
    p.then((response) => {
      dispatch(newVal(response));
      dispatch(asyncDone());
    });
  };
};
