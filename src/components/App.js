import React from "react";
import Container from "./Container";
import "./App.css";

const App = () => (
  <div className="App">
    <Container />
  </div>
);

export default App;
