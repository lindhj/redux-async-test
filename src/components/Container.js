import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchStuff } from "../actions";

const InnerContainer = ({ onClick, doingAsyncP, displayedValue }) => (
  <div>
    <div>{doingAsyncP ? "yes!" : "no"}</div>
    <div>{displayedValue}</div>
    <button onClick={onClick}>Send async</button>
  </div>
);

const mapStateToProps = state => (
  {
    doingAsyncP: state.doingAsyncOp,
    displayedValue: state.displayedValue
  }
);

const mapDispatchToProps = dispatch => (
  {
    onClick: () => {
      dispatch(fetchStuff());
    }
  }
);

InnerContainer.propTypes = {
  onClick: PropTypes.func.isRequired,
  doingAsyncP: PropTypes.bool.isRequired,
  displayedValue: PropTypes.number.isRequired
};

const Container = connect(
  mapStateToProps,
  mapDispatchToProps
)(InnerContainer);

export default Container;
